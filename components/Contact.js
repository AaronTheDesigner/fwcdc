
const Contact = () => {

    return (
        <div className="contact">
            <h2 className="red-1" >Contact</h2>
             <p>
                 Due to the current pandemic situation, we are currently not accepting walk in tours. However, feel free to call and schedule an appointment. Availabilities vary based on age, time of year, and programs of interest like NC Pre-K, Smart Start, and Private listings.
            </p>
            <div className="contact-links">
                <a href="tel:+1-704-379-7924">
                    <img src="/009-phone-call.svg" alt="call"/>
                </a>
                <a href="mailto:etoliver@fwcdc.org">
                    <img src="/010-gmail.svg" alt="email"/>
                </a>
            </div>
        </div>
    )

}

export default Contact;