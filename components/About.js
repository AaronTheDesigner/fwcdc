

const About = () => {

    return (
        <div className="about">
            <div className="about-motto">
                <img src="/007-like.svg" alt=""/>
                <h2 className="purple-1" >What we stand for</h2>                
                <img src="/008-mind.svg" alt=""/>
            </div>

            <p>
                Here at First Ward Child Development Center, we are committed to becoming a "totally inclusive school." By creating an environment free from the adult conventions of bias and prejudice, FWCDC children focus on similarities between people, rather than differences. We teach acceptance in a higly interactive environment. Total Inclusion - it's an idea we epitomize in our slogan.
                <br/>   
                <br/> 
                <span className="red-1" >Open Hearts</span> <span className="purple-1" >Open Minds</span>        
            </p>
            
        </div>
    )

}

export default About;