

const Footer = () => {
    return (
        <footer>
            <div className="container">
                <div className="footer">
                    <div className="address">
                        <address>
                            600 E 7th St, <br/>
                            Charlotte, NC 28202
                        </address>
                    </div>
                    <div className="symbol">
                        <img src="/Logo.svg" alt=""/>
                    </div>
                    <div className="contact">
                        <a href="tel:+1-704-379-7924">704-379-7924</a> <br/>
                        <a href="mailto:etoliver@fwcdc.org">etoliver@fwcdc.org</a>
                    </div>
                </div>
            </div>
        </footer>
    )

}


export default Footer;