
const Header = () => {

    return (
        <div className="header">
            <div className="pd-1">
                <div className="logo">
                    <img className="" src="/Logo.svg" alt="logo"/>
                    <div className="title">
                        <h1 className="blue-1 ">First Ward CDC</h1>
                    </div>
                </div>
                <span className="motto ">
                    <h2 className="red-1">
                        open hearts
                    </h2>
                    <h2 className="purple-1">
                        open minds
                    </h2>
                </span>
            </div>
        </div>
    )

}

export default Header;