
const Features = () => {

    return (
        <div className="features">
            <div className="column">
                <span>
                    <img src="/001-crown.svg" alt=""/>
                    <div className="description">
                        <h2>Centrally Located</h2>
                        <p>Located in the heart of Charlotte, North Carolina.</p>
                    </div>                    
                </span>
                <span>
                    <img src="/002-mask.svg" alt=""/>
                    <div className="description">
                        <h2>COVID Safe</h2>
                        <p>Our facility is up to date with all policies regarding COVID-19.</p>
                    </div>  
                    
                </span>
            </div>
            <div className="column">
                <span>
                    <img src="/003-best.svg" alt=""/>
                    <div className="description">
                        <h2>Five Star Rated</h2>
                        <p>We are the only five star rated child care center in the Uptown Charlotte area.</p>
                    </div>                
                </span>
                <span>
                    <img src="/004-mobile-app.svg" alt=""/>
                    <div className="description">
                        <h2>First Ward App</h2>
                        <p>Coming Soon: our First Ward App will consolidate all of a parent's needs.</p>
                    </div>                
                </span>
            </div>
        </div>
    )

}

export default Features;