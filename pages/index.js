import Head from 'next/head';
import Header from '../components/Header';
import About from '../components/About';
import Features from '../components/Features';
import Contact from '../components/Contact';
import Footer from '../components/Footer';

export default function Home() {
  return (
    <div>
      <Head>
        <title>First Ward Child Development Center</title>
        <link rel="icon" href="/Logo.svg" />
        <link rel="preconnect" href="https://fonts.gstatic.com"/>
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet"/>
      </Head>

      <div className="page">
          <Header />
        <div className="container">    

          <About />

          <Features />

          <Contact />
        </div>
          <Footer />
        
      </div>
    </div>
  )
}
